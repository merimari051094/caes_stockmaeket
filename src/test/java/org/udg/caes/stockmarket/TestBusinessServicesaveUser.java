package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;

/**
 * Created by Meriem i Laila on 12/1/2017.
 */
public class TestBusinessServicesaveUser {
    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Test
    public void saveUserOK(@Mocked final User u) {
        business_serv.saveUser(u);

        new Verifications(){{
            mPS.saveUser(u);
        }};
    }
}
