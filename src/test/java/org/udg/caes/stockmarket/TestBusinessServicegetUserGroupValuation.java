package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aula on 13/1/2017.
 */
public class TestBusinessServicegetUserGroupValuation {
    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Mocked UserGroup usgr1;

    @Test
    public void getUserGroupValuationOKZeroUserZeroGroup() throws Exception {
        final String id = "i";
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();

        new Expectations(business_serv){{
            mPS.getUserGroup(id); result = usgr1;
            usgr1.getUsers(); result = llistaU;
            usgr1.getGroups(); result = llistaG;
        }};

        business_serv.getUserGroupValuation(id);

        new Verifications(){{
            usgr1.getGroups(); times = 1;
            usgr1.getUsers(); times = 1;
        }};
    }

    @Test
    public void getUserGroupValuationOKOneUserZeroGroup() throws Exception {
        final String id = "i";
        final List<User> llistaU = new ArrayList<User>();
        final User u1 = new User("id");
        llistaU.add(u1);

        new Expectations(business_serv){{
            mPS.getUserGroup(id); result = usgr1;
            usgr1.getUsers(); result = llistaU;
        }};

        business_serv.getUserGroupValuation(id);

        new Verifications(){{
            usgr1.getUsers(); times=1;
        }};
    }

 }
