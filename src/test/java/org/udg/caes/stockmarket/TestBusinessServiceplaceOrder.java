package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

/**
 * Created by aula on 13/1/2017.
 */
public class TestBusinessServiceplaceOrder {

    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void placeOrderOK(@Mocked final Order o) throws NoSuchMethodException {
        business_serv.placeOrder(o);
        new Verifications(){{
            o.setId(anyString); times = 1;
            o.setStatus(Order.PROCESSING); times = 1;
            mPS.saveOrder(o);
            o.send(mBS);
        }};
    }

    @Test
    public void placeOrderNoSuchMethodException(@Mocked final Order o) throws NoSuchMethodException {
        new Expectations(){{
            o.send(mBS); result = new NoSuchMethodException();
        }};
        thrown.expect(NoSuchMethodException.class);
        thrown.reportMissingExceptionWithMessage("If no such method, there should be an exception");

        business_serv.placeOrder(o);
        new Verifications(){{
            o.setId(anyString); times = 1;
            o.setStatus(Order.PROCESSING); times = 1;
            mPS.saveOrder(o);
            o.send(mBS);
        }};
    }

}