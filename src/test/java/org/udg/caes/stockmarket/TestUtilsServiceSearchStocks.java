package org.udg.caes.stockmarket;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.StockMarket;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import javax.rmi.CORBA.Util;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;


public class TestUtilsServiceSearchStocks {

    @Injectable BrokerService mBS;
    @Injectable EventBus mEB;
    @Tested UtilsService uService;
    @Inject String s="new",s1="new",s2="new2";
    @Injectable StockMarket sMark;

    @Test
    public void searchStocksNoMatch() {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
        }};
        assertTrue(uService.searchStocks("nou").size()==0);
    }

    @Test
    public void searchStocksOkZeroStockMarkets() {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
        }};
        ArrayList<String> array=uService.searchStocks(s);
        new Verifications(){{
            mBS.getMarkets(); times = 1;
        }};
    }

    @Test
    public void searchStocksOkZeroStringOneStockMarket() {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
        }};
        ArrayList<String> array=uService.searchStocks(s);
        new Verifications(){{
            mBS.getMarkets(); times = stockMarkets.size();
        }};
    }

    @Test
    public void searchStocksOkOneStringOneStockMarket() {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
        }};
        ArrayList<String> array=uService.searchStocks(s);
        new Verifications(){{
            mBS.getMarkets(); times = 1;
            sMark.getAllStocks(); times=1;
        }};
    }

    @Test
    public void searchStocksOkOneStringMoreStockMarket() {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
        }};
        ArrayList<String> array=uService.searchStocks(s);
        new Verifications(){{
            mBS.getMarkets(); times = 1;
            sMark.getAllStocks(); times=1*stockMarkets.size();
        }};
    }

    @Test
    public void searchStocksOkMoreStringOneStockMarket() {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        stocks.add(s2);
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
        }};
        ArrayList<String> array=uService.searchStocks(s);
        new Verifications(){{
            mBS.getMarkets(); times = 1;
            sMark.getAllStocks(); times=1;
        }};
    }

    @Test
    public void searchStocksOkMoreStringMorStockMarket() {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        stocks.add(s2);
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
        }};
        ArrayList<String> array=uService.searchStocks(s);
        new Verifications(){{
            mBS.getMarkets(); times = 1;
            sMark.getAllStocks(); times=1*stockMarkets.size();
        }};
    }

}