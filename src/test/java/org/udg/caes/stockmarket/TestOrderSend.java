package org.udg.caes.stockmarket;

import com.google.common.eventbus.EventBus;
import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.FAKE.Fake_PS_MySQL;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import java.util.Date;
import java.util.NoSuchElementException;


public class TestOrderSend {

    User user_tst = new User("New");
    Portfolio p=new Portfolio(user_tst,"Lilly");
    Date l = new Date(20161611);

    @Tested Order order=new Order(p,10,l,2);
    @Injectable Order order2;
    @Mocked BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void sendOK() throws NoSuchMethodException {
        order.send(mBS);
        new Verifications() {{
            mBS.send(order); times=1;
        }};
    }
}