package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aula on 13/1/2017.
 */
public class TestBusinessServicegetProfitofPortfolioAndUser {

    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Mocked Stock e1;
    @Mocked Metall g1;
    @Mocked Metall s1;

    public void agregarElementsAlesLlistes(List<Stock> llistaE, int stock, List<Metall> llistaG, int gold, List<Metall> llistaS, int silver) {
        for (int i = 0; i<stock; i++) {
            llistaE.add(e1);
        }
        for (int i = 0; i<silver; i++) {
            llistaS.add(s1);
        }
        for (int i = 0; i<gold; i++) {
            llistaG.add(g1);
        }
    }

    @Test
    public void getProfitofPortfolioAndUserOneStockOneMetallOneGoldOneSilver(@Mocked final Portfolio p1) throws Exception {
        final List<Stock> llistaE = new ArrayList<Stock>();
        final List<Metall> llistaG = new ArrayList<Metall>();
        final List<Metall> llistaS = new ArrayList<Metall>();
        agregarElementsAlesLlistes(llistaE,1,llistaG,1,llistaS,1);

        new Expectations(){{
            p1.getStocks(); result = llistaE;
            p1.getGold(); result = llistaG;
            p1.getSilver(); result = llistaS;
        }};

        business_serv.getProfitofPortfolioAndUser(p1,0.0);

        new Verifications(){{
            p1.getStocks(); times=1;
            p1.getGold(); times = 1;
            p1.getSilver(); times = 1;
            e1.getQuantity(); times = 1;
            g1.getmAmountOz(); times = 1;
            s1.getmAmountOz(); times = 1;
            mBS.getPrice(anyString); times = 3;
            e1.getPrice(); times = 1;
            g1.getValue(); times = 1;
            s1.getValue(); times = 1;
        }};
    }

    @Test
    public void getProfitofPortfolioAndUserZeroStockZeroMetallZeroGoldZeroSilver(@Mocked final Portfolio p1) throws Exception {
        final List<Stock> llistaE = new ArrayList<Stock>();
        final List<Metall> llistaG = new ArrayList<Metall>();
        final List<Metall> llistaS = new ArrayList<Metall>();
        agregarElementsAlesLlistes(llistaE,0,llistaG,0,llistaS,0);

        business_serv.getProfitofPortfolioAndUser(p1,0.0);

        new Verifications(){{
            p1.getStocks(); times=1;
            p1.getGold(); times = 1;
            p1.getSilver(); times = 1;
            e1.getQuantity(); times = 0;
            g1.getmAmountOz(); times = 0;
            s1.getmAmountOz(); times = 0;
            mBS.getPrice(anyString); times = 0;
            e1.getPrice(); times = 0;
            g1.getValue(); times = 0;
            s1.getValue(); times = 0;
        }};
    }

    @Test
    public void getProfitofPortfolioAndUserMoreThanOneStockMoreThanOneMetallMoreThanOneGoldMoreThanOneSilver(@Mocked final Portfolio p1) throws Exception {
        final List<Stock> llistaE = new ArrayList<Stock>();
        final List<Metall> llistaG = new ArrayList<Metall>();
        final List<Metall> llistaS = new ArrayList<Metall>();
        agregarElementsAlesLlistes(llistaE,2,llistaG,2,llistaS,2);

        new Expectations(){{
            p1.getStocks(); result = llistaE;
            p1.getGold(); result = llistaG;
            p1.getSilver(); result = llistaS;
        }};

        business_serv.getProfitofPortfolioAndUser(p1,0.0);

        new Verifications(){{
            p1.getStocks(); times=1;
            p1.getGold(); times = 1;
            p1.getSilver(); times = 1;
            e1.getQuantity(); times = 2;
            g1.getmAmountOz(); times = 2;
            s1.getmAmountOz(); times = 2;
            mBS.getPrice(anyString); times = 6;
            e1.getPrice(); times = 2;
            g1.getValue(); times = 2;
            s1.getValue(); times = 2;
        }};
    }


}
