package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertSame;

public class TestUserGetPortfolio {

    @Tested User user_tst = new User("New");
    @Inject Portfolio p = new Portfolio(user_tst,"Lilly");
    @Inject Portfolio p1 = new Portfolio(user_tst,"Meri");

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void getPortfolioError() throws ElementNotExists, ElementAlreadyExists {
        user_tst.addPortfolio(p);
        new Expectations() {{
            p.getId(); result=new ElementNotExists();
        }};
        thrown.expect(ElementNotExists.class);
        thrown.reportMissingExceptionWithMessage("El portfolio no existeix");
        user_tst.getPortfolio("Meri");
    }

    @Test
    public void getPortfolioOkOne() throws ElementNotExists, ElementAlreadyExists {
        user_tst.addPortfolio(p);
        new Expectations() {{
            p.getId(); result="Lilly";
        }};
        Portfolio pS2=user_tst.getPortfolio("Lilly");
        assertSame("El portfolio existeix, s'ha trobat el portfolio", p, pS2);
    }

    @Test
    public void getPortfolioOkMore() throws ElementNotExists, ElementAlreadyExists {
        user_tst.addPortfolio(p);
        user_tst.addPortfolio(p1);
        new Expectations() {{
            p.getId(); result="Lilly";
        }};
        Portfolio pS2=user_tst.getPortfolio("Lilly");
        assertSame("El portfolio existeix, s'ha trobat el portfolio", p, pS2);
    }

}