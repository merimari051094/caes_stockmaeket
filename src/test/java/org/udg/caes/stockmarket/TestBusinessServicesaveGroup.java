package org.udg.caes.stockmarket;

import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;

/**
 * Created by Meriem i Laila on 12/1/2017.
 */
public class TestBusinessServicesaveGroup {
    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Test
    public void saveGroupOK(@Mocked final UserGroup u) {
        business_serv.saveGroup(u);

        new Verifications(){{
            mPS.saveUserGroup(u);
        }};
    }

}
