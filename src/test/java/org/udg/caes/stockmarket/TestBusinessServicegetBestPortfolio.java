package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNull;

/**
 * Created by aula on 13/1/2017.
 */
public class TestBusinessServicegetBestPortfolio {

    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();
    @Mocked User usr;
    @Mocked Portfolio p;

    @Test
    public void getBestPortfolioOKOnePortfolio() throws StockNotFound, EntityNotFound {
        final String id = "1";
        final List<Portfolio> llista = new ArrayList<Portfolio>();
        llista.add(p);
        new Expectations(business_serv){{
            mPS.getUser(id); result = usr;
            usr.getAllPortfolios(); result = llista;
            business_serv.getProfitofPortfolioAndUser(p,0); result=0;
        }};
        business_serv.getBestPortfolio(id);
    }

    @Test
    public void getBestPortfolioOKZeroPortfolio() throws StockNotFound, EntityNotFound {
        final String id = "1";
        final List<Portfolio> llista = new ArrayList<Portfolio>();
        new Expectations(business_serv){{
            mPS.getUser(id); result = usr;
            usr.getAllPortfolios(); result = llista;
        }};
        assertNull(business_serv.getBestPortfolio(id));
    }

    @Test
    public void getBestPortfolioEntityNotFound() throws EntityNotFound {
        final String id = "i";

        new Expectations(){{
            mPS.getUser(id); result = new EntityNotFound();
        }};

        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.getUser(id);
    }

    @Test
    public void getBestPortfolioStockNotFound() throws EntityNotFound, StockNotFound {
        final String id = "i";
        final List<Portfolio> llista = new ArrayList<Portfolio>();
        llista.add(p);
        new Expectations(business_serv){{
            mPS.getUser(id); result = usr;
            usr.getAllPortfolios(); result = llista;
            business_serv.getProfitofPortfolioAndUser(p,0); result=new StockNotFound();
        }};

        thrown.expect(StockNotFound.class);
        thrown.reportMissingExceptionWithMessage("If stock don't exist, there should be an exception");
        business_serv.getBestPortfolio(id);
    }
}
