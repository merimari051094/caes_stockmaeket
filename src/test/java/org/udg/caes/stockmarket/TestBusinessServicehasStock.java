package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Meriem i Laila on 12/1/2017.
 */
public class TestBusinessServicehasStock {
    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Mocked  Portfolio p;

    @Test
    public void hasStockOKOneStockTrue() throws EntityNotFound {
        final String id = "i";
        final String ticker = "x";
        final User u = new User(id);
        final List llista = new ArrayList<Portfolio>();
        final Portfolio p = new Portfolio(u,"maria");
        llista.add(p);

        new Expectations(u,p){{
            mPS.getUser(id); result = u;
            u.getAllPortfolios(); result = llista;
            p.hasStock(ticker); result = true;
        }};

        assertTrue(business_serv.hasStock(id,ticker));

        new Verifications(){{
            p.hasStock(ticker); times = llista.size();
        }};

    }

    @Test
    public void hasStockOKOneStockNotTrue() throws EntityNotFound {
        final String id = "i";
        final String ticker = "x";
        final User u = new User(id);
        final List llista = new ArrayList<Portfolio>();
        final Portfolio p = new Portfolio(u,"maria");
        llista.add(p);

        new Expectations(u,p){{
            mPS.getUser(id); result = u;
            u.getAllPortfolios(); result = llista;
            p.hasStock(ticker); result = false;
        }};

        assertFalse(business_serv.hasStock(id,ticker));

        new Verifications(){{
            p.hasStock(ticker); times = llista.size();
        }};

    }

    @Test
    public void hasStockOKZeroStock() throws EntityNotFound {
        final String id = "i";
        final String ticker = "x";
        final User u = new User(id);
        final List llista = new ArrayList<Portfolio>();

        new Expectations(u){{
            mPS.getUser(id); result = u;
            u.getAllPortfolios(); result = llista;
        }};

        business_serv.hasStock(id,ticker);

        new Verifications(){{
            p.hasStock(ticker); times = 0;
        }};
    }

    @Test
    public void hasStockOKMoreThanOneStock() throws EntityNotFound {
        final String id = "i";
        final String ticker = "x";
        final User u = new User(id);
        final List llista = new ArrayList<Portfolio>();
        llista.add(new Portfolio(u,"maria"));
        llista.add(new Portfolio(u,"cen"));

        new Expectations(u){{
            mPS.getUser(id); result = u;
            u.getAllPortfolios(); result = llista;
        }};

        business_serv.hasStock(id,ticker);

        new Verifications(){{
            p.hasStock(ticker); times = llista.size();
        }};
    }

    @Test
    public void hasStockEntityNotFound() throws EntityNotFound {
        final String id = "i";
        final String ticker = "x";
        final User u = new User(id);

        new Expectations(u){{
            mPS.getUser(id); result = new EntityNotFound();
        }};

        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.hasStock(id,ticker);
    }
}
