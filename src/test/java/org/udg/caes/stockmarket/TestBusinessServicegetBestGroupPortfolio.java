package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNull;

/**
 * Created by aula on 13/1/2017.
 */
public class TestBusinessServicegetBestGroupPortfolio {

    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Mocked UserGroup ug;
    @Mocked UserGroup ug2;
    @Mocked User usr;
    @Mocked Portfolio p;

    @Test
    public void getBestGroupPortfolioZeroUsersZeroGroups(@Mocked final Portfolio best) throws EntityNotFound, StockNotFound {
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();
        new Expectations(){{
            mPS.getUserGroup("i"); result = ug;
            ug.getUsers(); result = llistaU;
            ug.getGroups(); result = llistaG;
        }};
        business_serv.getBestGroupPortfolio("i",best,0.0);
        new Verifications(){{
            ug.getUsers(); times = 1;
            ug.getGroups(); times = 1;
        }};
    }

    @Test
    public void getBestGroupPortfolioZeroUsersOneGroups(@Mocked final Portfolio best) throws EntityNotFound, StockNotFound {
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();
        llistaG.add(ug2);
        new Expectations(business_serv){{
            mPS.getUserGroup("i"); result = ug;
            ug.getUsers(); result = llistaU;
            ug.getGroups(); result = llistaG;
            ug2.getId(); result = "a";
        }};
        business_serv.getBestGroupPortfolio("i",new Portfolio(new User("x"),"name"),0.0);
        new Verifications(){{
            ug.getUsers(); times = 1;
            ug.getGroups(); times = 1;
        }};
    }

    @Test
    public void getBestGroupPortfolioOneUsersZeroPortfolio(@Mocked final Portfolio best) throws EntityNotFound, StockNotFound {
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        llistaU.add(usr);
        new Expectations(){{
            mPS.getUserGroup("i"); result = ug;
            ug.getUsers(); result = llistaU;
            ug.getGroups(); result = llistaG;
            usr.getAllPortfolios(); result = llistaP;
        }};
        business_serv.getBestGroupPortfolio("i",best,0.0);
        new Verifications(){{
            ug.getUsers(); times = 1;
            ug.getGroups(); times = 1;
        }};
    }

    @Test
    public void getBestGroupPortfolioOneUsersOnePortfolio(@Mocked final Portfolio best) throws EntityNotFound, StockNotFound {
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        llistaU.add(usr);
        llistaP.add(best);
        new Expectations(business_serv){{
            mPS.getUserGroup("i"); result = ug;
            ug.getUsers(); result = llistaU;
            ug.getGroups(); result = llistaG;
            usr.getAllPortfolios(); result = llistaP;
            business_serv.getProfitofPortfolioAndUser(best,0); result = 0;
        }};
        business_serv.getBestGroupPortfolio("i",best,0.0);
        new Verifications(){{
            ug.getUsers(); times = 1;
            ug.getGroups(); times = 1;
            usr.getAllPortfolios(); times = 1;
        }};
    }

    @Test
    public void getBestGroupPortfolioOneUsersMoreThanOnePortfolio(@Mocked final Portfolio best) throws EntityNotFound, StockNotFound {
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        llistaU.add(usr);
        llistaP.add(best);
        llistaP.add(best);
        new Expectations(business_serv){{
            mPS.getUserGroup("i"); result = ug;
            ug.getUsers(); result = llistaU;
            ug.getGroups(); result = llistaG;
            usr.getAllPortfolios(); result = llistaP;
            business_serv.getProfitofPortfolioAndUser(best,0); result = 0;
        }};
        business_serv.getBestGroupPortfolio("i",best,0.0);
        new Verifications(){{
            ug.getUsers(); times = 1;
            ug.getGroups(); times = 1;
            usr.getAllPortfolios(); times = 1;
            business_serv.getProfitofPortfolioAndUser(best,0); times=2;
        }};
    }


    @Test
    public void getBestGroupPortfolioMoreThanOneUsersZeroPortfolio(@Mocked final Portfolio best) throws EntityNotFound, StockNotFound {
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        llistaU.add(usr);
        llistaU.add(usr);
        new Expectations(){{
            mPS.getUserGroup("i"); result = ug;
            ug.getUsers(); result = llistaU;
            ug.getGroups(); result = llistaG;
            usr.getAllPortfolios(); result = llistaP;
        }};
        business_serv.getBestGroupPortfolio("i",best,0.0);
        new Verifications(){{
            ug.getUsers(); times = 1;
            ug.getGroups(); times = 1;
            usr.getAllPortfolios(); times = 2;
        }};
    }

    @Test
    public void getBestGroupPortfolioMoreThanOneUsersOnePortfolio(@Mocked final Portfolio best) throws EntityNotFound, StockNotFound {
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        llistaU.add(usr);
        llistaU.add(usr);
        llistaP.add(best);
        new Expectations(business_serv){{
            mPS.getUserGroup("i"); result = ug;
            ug.getUsers(); result = llistaU;
            ug.getGroups(); result = llistaG;
            usr.getAllPortfolios(); result = llistaP;
            business_serv.getProfitofPortfolioAndUser(best,0); result = 0;
        }};
        business_serv.getBestGroupPortfolio("i",best,0.0);
        new Verifications(){{
            ug.getUsers(); times = 1;
            ug.getGroups(); times = 1;
            usr.getAllPortfolios(); times = 2;
        }};
    }

    @Test
    public void getBestGroupPortfolioMoreThanOneUsersMoreThanOnePortfolio(@Mocked final Portfolio best) throws EntityNotFound, StockNotFound {
        final List<User> llistaU = new ArrayList<User>();
        final List<UserGroup> llistaG = new ArrayList<UserGroup>();
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        llistaU.add(usr);
        llistaU.add(usr);
        llistaP.add(best);
        llistaP.add(best);
        new Expectations(business_serv){{
            mPS.getUserGroup("i"); result = ug;
            ug.getUsers(); result = llistaU;
            ug.getGroups(); result = llistaG;
            usr.getAllPortfolios(); result = llistaP;
            business_serv.getProfitofPortfolioAndUser(best,0); result = 0;
        }};
        business_serv.getBestGroupPortfolio("i",best,0.0);
        new Verifications(){{
            ug.getUsers(); times = 1;
            ug.getGroups(); times = 1;
            usr.getAllPortfolios(); times = 2;
            business_serv.getProfitofPortfolioAndUser(best,0); times=4;
        }};
    }

    @Test
    public void getBestGroupPortfolioEntityNotFound() throws EntityNotFound {
        final String id = "i";

        new Expectations(){{
            mPS.getUser(id); result = new EntityNotFound();
        }};

        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.getUser(id);
    }

    @Test
    public void getBestGroupPortfolioStockNotFound() throws EntityNotFound, StockNotFound {
        final String id = "i";
        final List<Portfolio> llista = new ArrayList<Portfolio>();
        llista.add(p);
        new Expectations(business_serv){{
            mPS.getUser(id); result = usr;
            usr.getAllPortfolios(); result = llista;
            business_serv.getProfitofPortfolioAndUser(p,0); result=new StockNotFound();
        }};

        thrown.expect(StockNotFound.class);
        thrown.reportMissingExceptionWithMessage("If stock don't exist, there should be an exception");
        business_serv.getBestPortfolio(id);
    }
}
