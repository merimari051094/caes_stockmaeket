package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

/**
 * Created by aula on 13/1/2017.
 */
public class TestBusinessServicegetUserValuation {
    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void getUserValuationOK() throws Exception {
        final String id = "i";
        final User us = new User(id);
        new Expectations(business_serv){{
            mPS.getUser(id); result = us;
        }};
        business_serv.getUserValuation(id);
        new Verifications(){{
            business_serv.getValorationOfUser(us,0.0); times = 1;
        }};
    }

    @Test
    public void getUserValuationEntityNotFound() throws Exception {
        final String id = "i";
        final User us = new User(id);
        new Expectations(business_serv){{
            mPS.getUser(id); result = us;
            business_serv.getValorationOfUser(us,anyDouble); result= new EntityNotFound();
        }};
        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.getUserValuation(id);
    }

    @Test
    public void getUserValuationStockNotFound() throws Exception {
        final String id = "i";
        final User us = new User(id);
        new Expectations(business_serv){{
            mPS.getUser(id); result = us;
            business_serv.getValorationOfUser(us,anyDouble); result= new StockNotFound();
        }};
        thrown.expect(StockNotFound.class);
        thrown.reportMissingExceptionWithMessage("If stock don't exist, there should be an exception");
        business_serv.getUserValuation(id);
    }
}