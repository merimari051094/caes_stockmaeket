package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meriem i Laila on 13/1/2017.
 */
public class TestBusinessServicegetValorationOfUser {
    @Tested
    BusinessService business_serv;

    @Injectable
    BrokerService mBS;
    @Injectable
    PersistenceService mPS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Mocked Portfolio p1;
    @Mocked Stock e1;
    @Mocked Metall g1;
    @Mocked Metall s1;
    @Mocked User u;

    public void agregarElementsAlesLlistes(List<Portfolio> llistaP, int portfolio, List<Stock> llistaE, int stock, List<Metall> llistaG, int gold, List<Metall> llistaS, int silver) {
        for (int i = 0; i<portfolio; i++)
            llistaP.add(p1);
        for (int i = 0; i<stock; i++)
            llistaE.add(e1);
        for (int i = 0; i<silver; i++)
            llistaS.add(s1);
        for (int i = 0; i<gold; i++)
            llistaG.add(g1);
    }

    public void comprovarPortfolio(final int portfolio, final int stock, final int silver, final int gold) throws StockNotFound {
        new Verifications(){{
            p1.getStocks(); times=portfolio;
            p1.getGold(); times = portfolio;
            p1.getSilver(); times = portfolio;
            e1.getQuantity(); times = stock*portfolio;
            g1.getmAmountOz(); times = gold*portfolio;
            s1.getmAmountOz(); times = silver*portfolio;
            mBS.getPrice(anyString); times = (stock + silver + gold)*portfolio;
        }};
    }

    @Test
    public void getUserValuationOKOnePortfolioOneStockOneMetallOneGoldOneSilver() throws Exception {
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        final List<Stock> llistaE = new ArrayList<Stock>();
        final List<Metall> llistaG = new ArrayList<Metall>();
        final List<Metall> llistaS = new ArrayList<Metall>();
        agregarElementsAlesLlistes(llistaP,1,llistaE,1,llistaG,1,llistaS,1);

        new Expectations(){{
            u.getAllPortfolios(); result = llistaP;
            p1.getStocks(); result = llistaE;
            p1.getGold(); result = llistaG;
            p1.getSilver(); result = llistaS;
        }};

        business_serv.getValorationOfUser(u,0.0);

        comprovarPortfolio(1,1,1,1);
    }

    @Test
    public void getUserValuationOKZeroPortfolioZeroStockZeroMetallZeroGoldZeroSilver() throws Exception {
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        final List<Stock> llistaE = new ArrayList<Stock>();
        final List<Metall> llistaG = new ArrayList<Metall>();
        final List<Metall> llistaS = new ArrayList<Metall>();
        agregarElementsAlesLlistes(llistaP,0,llistaE,0,llistaG,0,llistaS,0);

        new Expectations(){{
            u.getAllPortfolios(); result = llistaP;
        }};

        business_serv.getValorationOfUser(u,0.0);

        comprovarPortfolio(0,0,0,0);
    }

    @Test
    public void getUserValuationOKMoreThanOnePortfolioMoreThanOneStockMoreThanOneMetallMoreThanOneGoldMoreThanOneSilver() throws Exception {
        final List<Portfolio> llistaP = new ArrayList<Portfolio>();
        final List<Stock> llistaE = new ArrayList<Stock>();
        final List<Metall> llistaG = new ArrayList<Metall>();
        final List<Metall> llistaS = new ArrayList<Metall>();
        agregarElementsAlesLlistes(llistaP,2,llistaE,2,llistaG,2,llistaS,2);

        new Expectations(){{
            u.getAllPortfolios(); result = llistaP;
            p1.getStocks(); result = llistaE;
            p1.getGold(); result = llistaG;
            p1.getSilver(); result = llistaS;
        }};

        business_serv.getValorationOfUser(u,0.0);

        comprovarPortfolio(2,2,2,2);
    }
}
