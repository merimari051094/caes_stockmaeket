package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;

import static org.junit.Assert.assertSame;

/**
 * Created by aula on 12/1/2017.
 */
public class TestUserHasStock {

    @Tested User user_tst = new User("New");
    @Inject Portfolio p = new Portfolio(user_tst,"Lilly");
    @Inject Portfolio p1 = new Portfolio(user_tst,"Meri");
    @Inject Stock s= new Stock("new",40,10.3);
    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void hasStockError() throws ElementAlreadyExists {
        p.addStock(s);
        user_tst.addPortfolio(p);
        final String name = "nina";
        boolean has=user_tst.hasStock(name);
        assertSame("No s'ha trobat el stock", false, has);
    }

    @Test
    public void hasStockOk() throws ElementAlreadyExists {
        p.addStock(s);
        user_tst.addPortfolio(p);
        final String name = "new";
        boolean has=user_tst.hasStock(name);
        assertSame("S'ha trobat el stock", true, has);
    }

}