package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aula on 13/1/2017.
 */
public class TestBusinessServicegetCommonStocks {

    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Mocked User u1;
    @Mocked User u2;
    @Mocked Portfolio p;
    @Mocked ArrayList<String> stocks;

    @Test
    public void commonStocksOK () throws EntityNotFound {
        final List<Portfolio> llista = new ArrayList<Portfolio>();
        final List<Stock> llistaS = new ArrayList<Stock>();
        llista.add(p);
        llistaS.add(new Stock("i",50,30.0));
        new Expectations(){{
            mPS.getUser("i"); result  = u1;
            mPS.getUser("d"); result  = u2;
            u1.getAllPortfolios(); result = llista;
            p.getStocks(); result = llistaS;
        }};
        business_serv.getCommonStocks("i","d");
        new Verifications(){{
            p.getStocks(); times = llista.size();
            stocks.contains("i"); times = llista.size()*llistaS.size();
        }};
    }

    @Test
    public void commonStocksEntityNotFoundUser1 () throws EntityNotFound {
        final List<Portfolio> llista = new ArrayList<Portfolio>();
        final List<Stock> llistaS = new ArrayList<Stock>();
        llista.add(p);
        llistaS.add(new Stock("i", 50, 30.0));
        new Expectations() {{
            mPS.getUser("i"); result = new EntityNotFound();
            mPS.getUser("d"); result = u2;
        }};
        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.getCommonStocks("i", "d");
    }
    @Test
    public void commonStocksEntityNotFoundUser2 () throws EntityNotFound {
        final List<Portfolio> llista = new ArrayList<Portfolio>();
        final List<Stock> llistaS = new ArrayList<Stock>();
        llista.add(p);
        llistaS.add(new Stock("i", 50, 30.0));
        new Expectations() {{
            mPS.getUser("i"); result = u1;
            mPS.getUser("d"); result = new EntityNotFound();
        }};
        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.getCommonStocks("i", "d");
    }
    @Test
    public void commonStocksEntityNotFoundUser1User2 () throws EntityNotFound {
        final List<Portfolio> llista = new ArrayList<Portfolio>();
        final List<Stock> llistaS = new ArrayList<Stock>();
        llista.add(p);
        llistaS.add(new Stock("i", 50, 30.0));
        new Expectations() {{
            mPS.getUser("i"); result = new EntityNotFound();
            mPS.getUser("d"); result = new EntityNotFound();
        }};
        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.getCommonStocks("i", "d");
    }

}
