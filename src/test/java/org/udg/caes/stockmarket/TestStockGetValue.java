package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import mockit.Tested;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

public class TestStockGetValue {

    @Tested Stock s= new Stock("new",40,10.3);
    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void getValueOk() {
        double d=s.getValue();
        assertFalse("Dóna correcte el valor",d!=(40*10.3));
    }

    @Test
    public void getValueError() {
        double d=s.getValue();
        assertFalse("Dóna incorrecte el valor",d==0);
    }

}