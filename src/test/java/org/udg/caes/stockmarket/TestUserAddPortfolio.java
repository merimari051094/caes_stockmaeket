package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import mockit.Expectations;
import mockit.Tested;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

/**
 * Created by aula on 12/1/2017.
 */
public class TestUserAddPortfolio {

    @Tested User user_tst = new User("New");
    @Inject Portfolio p = new Portfolio(user_tst,"Lilly");
    @Inject Portfolio p1 = new Portfolio(user_tst,"Meri");

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void addPortfolioError() throws ElementAlreadyExists {
        user_tst.addPortfolio(p);
        new Expectations() {{
            user_tst.hasPortfolio(p.getId()); result=new ElementAlreadyExists();
        }};
        thrown.expect(ElementAlreadyExists.class);
        thrown.reportMissingExceptionWithMessage("El portfolio ja existeix");
        user_tst.addPortfolio(p);
    }

    @Test
    public void addPortfolioOk() throws ElementAlreadyExists {
        new Expectations() {{
            p.getId(); result="Lilly";
        }};
        user_tst.addPortfolio(p);
    }

}