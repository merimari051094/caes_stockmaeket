package org.udg.caes.stockmarket;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.StockMarket;
import org.udg.caes.stockmarket.exceptions.BrokerInternalError;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;

/**
 * Created by aula on 12/1/2017.
 */
public class TestUtilsServiceSearchStocksWithValue {

    @Injectable BrokerService mBS;
    @Injectable EventBus mEB;
    @Tested UtilsService uService;
    @Mocked
    String s="nina",s1="new",s2="new2";
    @Injectable StockMarket sMark;
    @Rule
    public ExpectedException thrown= ExpectedException.none();


    @Test //Zero stockMarket, 0 String
    public void searchStocksWithValueZeroStringsZeroStocks() throws StockNotFound, BrokerInternalError {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
        }};
        List<String> array=uService.searchStocksWithValue(10);
        new Verifications(){{
            mBS.getMarkets(); times = 1;
        }};
    }

    @Test //More stockMarket, 0 String
    public void searchStocksWithValueZeroStringsMoreStocks() throws StockNotFound, BrokerInternalError {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
        }};
        List<String> stockValue=uService.searchStocksWithValue(12);
        assertTrue(stockValue.size()==0);
    }

    @Test //1 stockMarket, 0 String
    public void searchStocksWithValueZeroStringsOneStocks() throws StockNotFound, BrokerInternalError {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
        }};
        List<String> stockValue=uService.searchStocksWithValue(12);
        assertTrue(stockValue.size()==0);
    }

    @Test // More stockMarket, 1 string, ok
    public void searchStocksOkOneStringMoreStockMarket() throws BrokerInternalError, StockNotFound {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
            mBS.getPrice(s1); result=12;
        }};
        List<String> array=uService.searchStocksWithValue(10);
        assertTrue(array.size()==1*stockMarkets.size());
    }

    @Test //1 stockMarket, 1 String,  OK
    public void searchStocksWithValueOkOneStringOneStocks() throws BrokerInternalError, StockNotFound {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
            mBS.getPrice(s1); result=10;
        }};
        List<String> stockValue=uService.searchStocksWithValue(8);
        assertTrue(stockValue.size()==1);
    }

    @Test //More stockMarket, More String,  OK
    public void searchStocksWithValueOkMoreStringMoreStocks() throws BrokerInternalError, StockNotFound {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        stocks.add(s2);
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
            mBS.getPrice(s1); result=12;
            mBS.getPrice(s2); result=14;
        }};
        List<String> array=uService.searchStocksWithValue(10);
        assertTrue(array.size()==stocks.size()*stockMarkets.size());
    }

    @Test //1 stockMarket, More String,  OK
    public void searchStocksWithValueOkMoreStringOneStocks() throws BrokerInternalError, StockNotFound {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        stocks.add(s1);
        new Expectations() {{
            mBS.getMarkets(); result=stockMarkets;
            sMark.getAllStocks(); result=stocks;
            mBS.getPrice(s1); result=10;
        }};
        List<String> stockValue=uService.searchStocksWithValue(8);
        new Verifications() {{
            mBS.getMarkets();  times=1;
            sMark.getAllStocks(); times=1;
            mBS.getPrice(s1); times=stocks.size();
        }};
    }

    @Test //1 stocMarket, 1 String, No Match
    public void searchStocksWithValueNoMatchOneStringsOneStocks() throws BrokerInternalError, StockNotFound {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        new Expectations() {{
                mBS.getMarkets(); result = stockMarkets;
                sMark.getAllStocks(); result = stocks;
                mBS.getPrice(s1); result = new StockNotFound();
        }};
        thrown.expect(BrokerInternalError.class);
        thrown.reportMissingExceptionWithMessage("No existeix cap stock");
        List<String> stockValue=uService.searchStocksWithValue(12);
    }

    @Test //More stocMarket, 1 String, No Match
    public void searchStocksWithValueNoMatchOneStringsMoreStocks() throws BrokerInternalError, StockNotFound {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        new Expectations() {{
            mBS.getMarkets(); result = stockMarkets;
            sMark.getAllStocks(); result = stocks;
            mBS.getPrice(s1); result = 10;
        }};
        List<String> stockValue=uService.searchStocksWithValue(12);
        assertTrue(stockValue.size()==0);
    }

    @Test //More stocMarket, More String, No Match
    public void searchStocksWithValueNoMatchMoreStringsMoreStocks() throws BrokerInternalError, StockNotFound {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        stocks.add(s2);
        new Expectations() {{
            mBS.getMarkets(); result = stockMarkets;
            sMark.getAllStocks(); result = stocks;
            mBS.getPrice(s1); result = 10;
            mBS.getPrice(s2); result = 8;
        }};
        List<String> stockValue=uService.searchStocksWithValue(12);
        assertTrue(stockValue.size()==0);
    }

    @Test //1 stocMarket, More String, No Match
    public void searchStocksWithValueNoMatchMoreStringsOneStocks() throws BrokerInternalError, StockNotFound {
        final List<StockMarket> stockMarkets = new ArrayList<StockMarket>();
        stockMarkets.add(sMark);
        final List<String> stocks= new ArrayList<String>();
        stocks.add(s1);
        stocks.add(s2);
        new Expectations() {{
            mBS.getMarkets(); result = stockMarkets;
            sMark.getAllStocks(); result = stocks;
            mBS.getPrice(s1); result = 10;
            mBS.getPrice(s2); result = 8;
        }};
        List<String> stockValue=uService.searchStocksWithValue(12);
        assertTrue(stockValue.size()==0);
    }

}