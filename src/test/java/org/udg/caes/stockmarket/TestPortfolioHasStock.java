package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import mockit.Tested;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;

import static org.junit.Assert.assertSame;

/**
 * Created by aula on 12/1/2017.
 */
public class TestPortfolioHasStock {

    @Inject User user_tst = new User("New");
    @Tested Portfolio p = new Portfolio(user_tst,"Lilly");
    @Inject Stock s= new Stock("new",40,10.3);

    @Test
    public void hasStockError() {
        p.addStock(s);
        final String name = "nina";
        boolean has=p.hasStock(name);
        assertSame("No s'ha trobat el stock", false, has);
    }

    @Test
    public void hasStockOk() {
        p.addStock(s);
        final String name = "new";
        boolean has=p.hasStock(name);
        assertSame("S'ha trobat el stock", true, has);
    }

}