package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;

import static org.junit.Assert.*;

/**
 * Created by aula on 12/1/2017.
 */
public class TestPortfolioGetBuyPrice {

    @Inject User user_tst = new User("New");
    @Tested Portfolio p = new Portfolio(user_tst,"Lilly");
    @Inject Stock s= new Stock("new",40,9.5);
    @Mocked Metall gold = new Metall(20,40,"GOLD");
    @Mocked Metall silver = new Metall(10,30,"SILVER");
    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void getBuyPriceOk() {
        p.addStock(s);
        p.addGold(gold);
        p.addSilver(silver);
        new Expectations() {{
            s.getValue(); result=380;
            gold.getValue(); result=800;
            silver.getValue(); result=300;
        }};
        double d=p.getBuyPrice();
        assertFalse("En el portfolio no existeix ni golds ni silvers",d!=1480.0);
    }

    @Test
    public void getBuyPriceNoGoldIStock() {
        p.addSilver(silver);
        new Expectations() {{
            silver.getValue(); result=300;
        }};
        double d=p.getBuyPrice();
        assertFalse("En el portfolio no existeix ni golds ni stocks",d!=300.0);
    }

    @Test
    public void getBuyPriceNotGoldISIlver() {
        p.addStock(s);
        new Expectations() {{
            s.getValue(); result=380;
        }};
        double d=p.getBuyPrice();
        assertFalse("En el portfolio no existeix ni golds ni silvers",d!=380.0);
    }

    @Test
    public void getBuyPriceNotStocks() {
        new Expectations() {{
            s.getValue(); result=0.0;
        }};
        assertNotEquals("En el portfolio no existeixen stocks", 0.1, p.getBuyPrice());
    }



}