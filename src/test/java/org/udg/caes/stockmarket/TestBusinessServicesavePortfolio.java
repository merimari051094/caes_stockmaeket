package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.InvalidOperation;

/**
 * Created by Meriem i Laila on 12/1/2017.
 */
public class TestBusinessServicesavePortfolio {
    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void savePortfolioOK(@Mocked final Portfolio p) throws ElementNotExists, InvalidOperation, ElementAlreadyExists, EntityNotFound {
        business_serv.savePortfolio(p);

        new Verifications(){{
            mPS.savePortfolio(p);
        }};
    }

    @Test
    public void savePortfolioElementNotExists(@Mocked final Portfolio p) throws ElementNotExists, InvalidOperation, ElementAlreadyExists, EntityNotFound {
        new Expectations(){{
            mPS.savePortfolio(p); result = new ElementNotExists();
        }};

        thrown.expect(ElementNotExists.class);
        thrown.reportMissingExceptionWithMessage("If portfolio don't exist, there should be an exception");
        business_serv.savePortfolio(p);

        new Verifications(){{
            mPS.savePortfolio(p);
        }};
    }

    @Test
    public void savePortfolioInvalidOperation(@Mocked final Portfolio p) throws ElementNotExists, InvalidOperation, ElementAlreadyExists, EntityNotFound {
        new Expectations(){{
            mPS.savePortfolio(p); result = new InvalidOperation();
        }};

        thrown.expect(InvalidOperation.class);
        thrown.reportMissingExceptionWithMessage("If it is a invalid operation, there should be an exception");
        business_serv.savePortfolio(p);

        new Verifications(){{
            mPS.savePortfolio(p);
        }};
    }

    @Test
    public void savePortfolioElementAlreadyExists(@Mocked final Portfolio p) throws ElementNotExists, InvalidOperation, ElementAlreadyExists, EntityNotFound {
        new Expectations(){{
            mPS.savePortfolio(p); result = new ElementAlreadyExists();
        }};

        thrown.expect(ElementAlreadyExists.class);
        thrown.reportMissingExceptionWithMessage("If portfolio exist, there should be an exception");
        business_serv.savePortfolio(p);

        new Verifications(){{
            mPS.savePortfolio(p);
        }};
    }

    @Test
    public void savePortfolioEntityNotFound(@Mocked final Portfolio p) throws ElementNotExists, InvalidOperation, ElementAlreadyExists, EntityNotFound {
        new Expectations(){{
            mPS.savePortfolio(p); result = new EntityNotFound();
        }};

        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If entity don't exist, there should be an exception");
        business_serv.savePortfolio(p);

        new Verifications(){{
            mPS.savePortfolio(p);
        }};
    }

}
