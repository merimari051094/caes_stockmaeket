package org.udg.caes.stockmarket;

import mockit.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertSame;

/**
 * Created by aula on 12/1/2017.
 */
public class TestUserHasPortfolio {

    @Tested User user_tst = new User("New");
    @Inject Portfolio p = new Portfolio(user_tst,"Lilly");
    @Inject Portfolio p1 = new Portfolio(user_tst,"Meri");

    @Test
    public void hasPortfolioErrorNoIgual() throws ElementAlreadyExists {
        user_tst.addPortfolio(p);
        new Expectations() {{
            p.getId(); result="Lilly";
        }};
        boolean has=user_tst.hasPortfolio("Meri");
        assertSame("No s'ha trobat el portfolio", false, has);
    }

    @Test
    public void hasPortfolioErrorNoHiHaPortFolio() {
        boolean has=user_tst.hasPortfolio("Meri");
        assertSame("No s'ha trobat el portfolio", false, has);
    }

    @Test
    public void hasPortfolioOkOne() throws ElementAlreadyExists {
        user_tst.addPortfolio(p);
        new Expectations() {{
            p.getId(); result="Lilly";
        }};
        boolean has=user_tst.hasPortfolio("Lilly");
        assertSame("S'ha trobat el portfolio", true, has);
    }

    @Test
    public void hasPortfolioOkMore() throws ElementAlreadyExists {
        user_tst.addPortfolio(p);
        user_tst.addPortfolio(p1);
        new Expectations() {{
            p.getId(); result="Lilly";
        }};
        boolean has=user_tst.hasPortfolio("Lilly");
        assertSame("S'ha trobat el portfolio", true, has);
    }
}