package org.udg.caes.stockmarket;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import mockit.*;
import org.junit.Test;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.StockMarket;
import org.udg.caes.stockmarket.FAKE.Fake_PS_MySQL;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;


public class TestUtilsServiceProcessOrderStatusCharge {

    @Injectable BrokerService mBS;
    @Injectable EventBus mEB;
    @Tested UtilsService uService;
    @Mocked Order order;

    @Test
    public void processOrderOkProcessing() {
        final Fake_PS_MySQL ps = new Fake_PS_MySQL();
        new Expectations() {{
            order.getStatus(); result=2;
        }};
        uService.processOrderStatusChange(order);
        new Verifications() {{
            ps.saveOrder(order); times=2;
        }};
    }

    @Test
    public void processOrderNoProcessing_Completed_PartiallyCompleted() {
        final Fake_PS_MySQL ps = new Fake_PS_MySQL();
        new Expectations() {{
            order.getStatus(); result=1;
        }};
        uService.processOrderStatusChange(order);
        new Verifications() {{
            ps.saveOrder(order); times=1;
        }};
    }

    @Test
    public void processOrderOkPartiallyCompleted() {
        final Fake_PS_MySQL ps = new Fake_PS_MySQL();
        new Expectations() {{
            order.getStatus(); result=4;
        }};
        uService.processOrderStatusChange(order);
        new Verifications() {{
            ps.saveOrder(order); times=1;
        }};
    }

    @Test
    public void processOrderOkCompleted() {
        final Fake_PS_MySQL ps = new Fake_PS_MySQL();
        new Expectations() {{
            order.getStatus(); result=3;
        }};
        uService.processOrderStatusChange(order);
        new Verifications() {{
            ps.saveOrder(order); times=1;
        }};
    }
}