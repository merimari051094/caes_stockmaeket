package org.udg.caes.stockmarket;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import javax.sound.sampled.Port;

/**
 * Created by aula on 12/1/2017.
 */
public class TestBusinessServicegetPortfolio {

    @Tested
    BusinessService business_serv;

    @Injectable
    PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void getPortfolioOK() throws EntityNotFound, ElementNotExists, ElementAlreadyExists {
        final String id = "i";
        final String name = "maria";
        final User u = new User("i");
        final Portfolio p = new Portfolio(u,name);

        new Expectations(u){{
            mPS.getUser(id); result = u;
            u.getPortfolio(name); result = p;
        }};

        business_serv.getPortfolio(id,name);
    }

    @Test
    public void getPortfolioEntityNotFound() throws EntityNotFound, ElementNotExists {
        final String id = "i";
        final String name = "maria";

        new Expectations(){{
            mPS.getUser(id); result = new EntityNotFound();
        }};

        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.getPortfolio(id,name);
    }

    @Test
    public void getPortfolioElementNotExists() throws EntityNotFound, ElementNotExists {
        final String id = "i";
        final String name = "maria";
        final User u = new User("i");

        new Expectations(u){{
            mPS.getUser(id); result = u;
            u.getPortfolio(name); result = new ElementNotExists();
        }};

        thrown.expect(ElementNotExists.class);
        thrown.reportMissingExceptionWithMessage("If portfolio don't exist in this user, there should be an exception");
        business_serv.getPortfolio(id,name);
    }
}
