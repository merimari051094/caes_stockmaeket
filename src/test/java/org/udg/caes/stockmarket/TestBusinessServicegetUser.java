package org.udg.caes.stockmarket;

import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import org.junit.Rule;
import mockit.Expectations;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import javax.inject.Inject;

/**
 * Created by aula on 12/1/2017.
 */
public class TestBusinessServicegetUser {

    @Tested
    BusinessService business_serv;

    @Injectable PersistenceService mPS;
    @Injectable
    BrokerService mBS;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void getUserOK() throws EntityNotFound {
        final String id = "i";

        new Expectations(){{
            mPS.getUser(id); result = new User("i");
        }};

        business_serv.getUser(id);
    }

    @Test
    public void getUserEntityNotFound() throws EntityNotFound {
        final String id = "i";

        new Expectations(){{
            mPS.getUser(id); result = new EntityNotFound();
        }};

        thrown.expect(EntityNotFound.class);
        thrown.reportMissingExceptionWithMessage("If user don't exist, there should be an exception");
        business_serv.getUser(id);
    }
}
