package org.udg.caes.stockmarket;

/**
 * Created by aula on 13/1/2017.
 */
public interface Material {

    public double getPrice();

    public void setPrice(double Price);

    public double getValue();
}
