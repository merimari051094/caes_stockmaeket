package org.udg.caes.stockmarket;

/**
 * Created by imartin on 14/12/16.
 */
public class Metall implements Material {
  double mAmountOz;
  double mPriceOz;
  String tipus;

  public Metall(double mAmountOz, double mPriceOz,String tipus) {
    this.mAmountOz = mAmountOz;
    this.mPriceOz = mPriceOz;
      this.tipus=tipus;
  }

  public double getmAmountOz() {
    return mAmountOz;
  }

  public void setmAmountOz(double mAmountOz) {
    this.mAmountOz = mAmountOz;
  }

  public double getPrice() {
    return mPriceOz;
  }

  public void setPrice(double Price) {
    this.mPriceOz = Price;
  }

  public double getValue() {
    return mAmountOz * mPriceOz;
  }
}
