package org.udg.caes.stockmarket;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 22/10/13
 * Time: 13:31
 * To change this template use File | Settings | File Templates.
 */
public class Stock implements Material{
  private String name;

  private int mQuantity;
  private double mBuyPrice;

    public Stock(String name, int quantity, double price) {
    this.name = name;
    this.mQuantity = quantity;
    this.mBuyPrice = price;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getQuantity() {
    return mQuantity;
  }

  public void setQuantity(int quantity) {
    this.mQuantity = quantity;
  }

  public double getPrice() {
    return mBuyPrice;
  }

  public void setPrice(double Price) {
    this.mBuyPrice = Price;
  }

  public double getValue() { return mQuantity * mBuyPrice; }


}
