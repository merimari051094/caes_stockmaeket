package org.udg.caes.stockmarket;

/**
 * Created by imartin on 15/12/16.
 */
public class OrderCompletedEvent {
  private Order mOrder;

  public OrderCompletedEvent(Order order) {
    mOrder = order;
  }

  public Order getOrder() {
    return mOrder;
  }
}
