package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.EXTERNAL.BrokerService;

import java.util.Date;

/**
 * Created by imartin on 12/12/13.
 */
public class Order {
  private String id;

  Portfolio portfolio;

  int type;
  public static int MARKET = 1;
  public static int STOP_LOSS = 2;

  // Amount of stocks bought
  int quantity;
  double targetPrice;
  // Limit date to execut the order
  Date limit;
  // Company to buy
  String ticker;

  int status;
  public static int ACCEPTED = 1;
  public static int PROCESSING = 2;
  public static int COMPLETED = 3;
  public static int PARTIALLY_COMPLETED = 4;
  public static int CANCELLED = 5;
  public static int TIMEOUT = 6;
  public static int REJECTED = 7;

  int orderType;
  public static int STOCK = 1;
  public static int GOLD = 2;
  public static int SILVER = 3;

  // Amount of stocks actually bought
  private int efQuant;
  // Buy price
  private double efPrice;
  // Price to trigger the loss order
  private double lossPrice;

  public Order(Portfolio pf, int q, double lp, Date li, String t) {
    status = Order.PROCESSING;
    portfolio = pf;
    quantity = q;
    type = STOP_LOSS;
    lossPrice = lp;
    ticker = t;
    limit = li;
    orderType = STOCK;
  }

  public Order(Portfolio pf, int q, Date l, String t) {
    status = Order.PROCESSING;
    portfolio = pf;
    quantity = q;
    type = MARKET;
    ticker = t;
    limit = l;
    orderType = STOCK;
  }

  public Order(Portfolio pf, int q, Date l, int material) {
    status = Order.PROCESSING;
    portfolio = pf;
    quantity = q;
    type = MARKET;
    limit = l;
    orderType = material;
  }


  /**
   * Send the order and a callback to be used once the stock service receives the response from the broker
   * @param bs
   * @return true if the order has been accepted
   * @throws NoSuchMethodException
   */
  public void send(BrokerService bs) throws NoSuchMethodException {
    bs.send(this);
  }

  public String getId() {
    return id;
  }

  public void setId(String i) {
    this.id = i;
  }

  public int getOrderType() { return orderType; }

  public String getTicker() {
    return ticker;
  }

  public int getEfQuant() {
    return efQuant;
  }

  public double getEfPrice() {
    return efPrice;
  }

  public Portfolio getPortfolio() {
    return portfolio;
  }

  public int getStatus() {
    return status;
  }

  public void setEfQuant(int eq) {
    this.efQuant = eq;
  }

  public void setEfPrice(double ep) {
    this.efPrice = ep;
  }

  public void setStatus(int s) {
    this.status = s;
  }

  public int getQuantity() {
    return quantity;
  }

  public int getType() {
    return type;
  }

  public double getLossPrice() {
    return lossPrice;
  }
}
