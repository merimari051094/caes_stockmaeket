package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains the business logic of the app.
 * It resembles a EJB from Java EE 7
 */
public class BusinessService {
  PersistenceService mPS;
  BrokerService mBS;

  @Inject
  public BusinessService(PersistenceService ps, BrokerService bs) {
    mPS = ps;
    mBS = bs;
  }

  private Integer orderId = 0;

  public User getUser(String id) throws EntityNotFound {
    return mPS.getUser(id);
  }

  public Portfolio getPortfolio(String userId, String name) throws EntityNotFound, ElementNotExists {
    User u = mPS.getUser(userId);
    return u.getPortfolio(name);
  }

  public void saveUser(User u) {
    mPS.saveUser(u);
  }
  public void saveGroup(UserGroup g) { mPS.saveUserGroup(g); }

  public void savePortfolio(Portfolio p) throws ElementNotExists, EntityNotFound, ElementAlreadyExists, InvalidOperation {
    mPS.savePortfolio(p);
  }

  public Boolean hasStock(String userId, String ticker) throws EntityNotFound {
    User u = mPS.getUser(userId);
    for (Portfolio p: u.getAllPortfolios()) {
      if (p.hasStock(ticker))
        return true;
    }
    return false;
  }

  // Computes the real-time value of the users assets
  public Double getUserValuation(String userId) throws Exception {
    User u = mPS.getUser(userId);
    double v = 0.0;
      v = getValorationOfUser(u, v);

      return v;
  }

    public double getValorationOfUser(User u, double v) throws StockNotFound {
        for (Portfolio p: u.getAllPortfolios()) {
          for (Stock s : p.getStocks())
            v += s.getQuantity() * mBS.getPrice(s.getName());
          for (Metall g : p.getGold())
            v += g.getmAmountOz() * mBS.getPrice("GOLD");
          for (Metall s : p.getSilver())
            v += s.getmAmountOz() * mBS.getPrice("SILVER");
        }
        return v;
    }

    // Computes the real-time value of the group assets
  public Double getUserGroupValuation(String userId) throws Exception {
    UserGroup ug = mPS.getUserGroup(userId);
    double v = 0.0;
    for (User u: ug.getUsers()) {
        v = getValorationOfUser(u, v);
    }
    for (UserGroup uu: ug.getGroups())
      v += this.getUserGroupValuation(uu.getId());

    return v;
  }

  // Finds the portfolio with the best real-time valuation
  public Portfolio getBestPortfolio(String userId) throws EntityNotFound, StockNotFound {
    User u = mPS.getUser(userId);
    double max = -Double.MAX_VALUE;
    Portfolio result = null;
    for (Portfolio p: u.getAllPortfolios()) {
      double profit = 0.0;
        profit = getProfitofPortfolioAndUser(p, profit);

      if (profit > max) {
        max = profit;
        result = p;
      }
    }
    return result;
  }

  // Finds the portfolio with the best real-time valuation
  public Portfolio getBestGroupPortfolio(String groupId, Portfolio pBest, double value) throws EntityNotFound, StockNotFound {
    UserGroup ug = mPS.getUserGroup(groupId);
    double max = value;
    for (User u : ug.getUsers()) {
      for (Portfolio p : u.getAllPortfolios()) {
        double profit = 0.0;
          profit = getProfitofPortfolioAndUser(p, profit);

        if (profit > max) {
          max = profit;
          pBest = p;
        }
      }
    }

    for (UserGroup uu: ug.getGroups()) {
      pBest = this.getBestGroupPortfolio(uu.getId(), pBest, max);
    }

    return pBest;
  }

    public double getProfitofPortfolioAndUser(Portfolio p, double profit) throws StockNotFound {
        for (Stock s : p.getStocks())
          profit += s.getQuantity() * (mBS.getPrice(s.getName()) - s.getPrice());
        for (Metall g : p.getGold())
          profit += g.getmAmountOz() * (mBS.getPrice("GOLD") - g.getValue());
        for (Metall s : p.getSilver())
          profit += s.getmAmountOz() * (mBS.getPrice("SILVER") - s.getValue());
        return profit;
    }


    public List<String> getCommonStocks(String userId1, String userId2) throws EntityNotFound {
    User u1 = mPS.getUser(userId1);
    User u2 = mPS.getUser(userId2);
    ArrayList<String> stocks = new ArrayList<String>();
    for (Portfolio p: u1.getAllPortfolios()) {
      for (Stock s : p.getStocks())
        if (!stocks.contains(s.getName()) && u2.hasStock(s.getName()))
          stocks.add(s.getName());
    }
    return stocks;
  }

  /**
   * This method sends an Order for a give User
   *
   * @param o Order
   * @throws OrderNotSent
   */
  public void placeOrder(Order o) throws NoSuchMethodException {
    orderId++;
    o.setId(orderId.toString());
    o.setStatus(Order.PROCESSING);
    mPS.saveOrder(o);
    o.send(mBS);
  }

}
